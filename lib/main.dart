import 'package:flutter/material.dart';

import 'src/app.dart';

void main() {
  final MyApp myApp = MyApp();

  runApp(
    myApp,
  );
}
